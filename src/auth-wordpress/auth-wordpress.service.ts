import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SocialInterface } from '../social/interfaces/social.interface';
import { AuthWordpressLoginDto } from './dto/auth-wordpress-login.dto';
import { AllConfigType } from '../config/config.type';
import { OAuth2Client } from 'google-auth-library';
const WPAPI = require('wpapi');

@Injectable()
export class AuthWordpressService {
  private google: OAuth2Client;
  private wp: any;

  constructor(private configService: ConfigService<AllConfigType>) {
    this.google = new OAuth2Client(
      configService.get('google.clientId', { infer: true }),
      configService.get('google.clientSecret', { infer: true }),
    );

    this.wp = new WPAPI({
      endpoint: 'http://src.wordpress-develop.dev/wp-json',
    });
  }

  async getProfileByToken(
    loginDto: AuthWordpressLoginDto,
  ): Promise<SocialInterface> {
    this.wp.posts().get()
      .then(function( data ) {
        // do something with the returned posts
      })
      .catch(function( err ) {
        // handle error
      });

    return {
      id: '1',
      email: '1',
      firstName: '1',
      lastName: '1',
    };
  }
}
