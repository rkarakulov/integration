import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from '../auth/auth.service';
import { AuthWordpressService } from './auth-wordpress.service';
import { LoginResponseType } from '../auth/types/login-response.type';
import { AuthWordpressLoginDto } from './dto/auth-wordpress-login.dto';

@ApiTags('Auth')
@Controller({
  path: 'auth/wordpress',
  version: '1',
})
export class AuthWordpressController {
  constructor(
    private readonly authService: AuthService,
    private readonly authWordpressService: AuthWordpressService,
  ) {}

  @Post('login')
  @HttpCode(HttpStatus.OK)
  async login(
    @Body() loginDto: AuthWordpressLoginDto,
  ): Promise<LoginResponseType> {
    const socialData = await this.authWordpressService.getProfileByToken(
      loginDto,
    );

    return this.authService.validateSocialLogin('wordpress', socialData);
  }
}
