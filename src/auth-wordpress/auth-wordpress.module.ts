import { Module } from '@nestjs/common';
import { AuthWordpressController } from './auth-wordpress.controller';
import { AuthWordpressService } from './auth-wordpress.service';
import { IsExist } from '../utils/validators/is-exists.validator';
import { IsNotExist } from '../utils/validators/is-not-exists.validator';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [ConfigModule, AuthModule],
  controllers: [AuthWordpressController],
  providers: [IsExist, IsNotExist, AuthWordpressService],
  exports: [AuthWordpressService],
})
export class AuthWordpressModule {}
