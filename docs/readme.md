# NestJS Boilerplate Documentation

---

## Table of Contents

- [Introduction](docs/introduction.md)
- [Installing and Running](docs/installing-and-running.md)
- [Working with database](docs/database.md)
- [Auth](docs/auth.md)
- [Serialization](docs/serialization.md)
- [File uploading](docs/file-uploading.md)
